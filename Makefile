installers != find * -maxdepth 0 -type d
cleaners   != printf '%s_clean ' $(installers)

help:
	# +-----------------------------+
	# This will overwrite your files!
	# Be 100% sure that you want this
	#
	# examples:
	#   make bash
	#   make bash_clean
	#   make all
	#   make clean

$(installers):
	@printf '\e[1;32m$@\e[0m:\n'
	@cd $@; make -s install

$(cleaners):
	@printf '\e[1;31m$@\e[0m:\n'
	@cd ${@:_clean=}; make -s clean

all: $(installers)
clean: $(cleaners)

.PHONY: help all clean $(installers) $(cleaners)
