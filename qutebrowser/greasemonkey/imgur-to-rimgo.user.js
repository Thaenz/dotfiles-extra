// ==UserScript==
// @name          imgur to rimgo
// @namespace     -
// @description   Redirects imgur to rimgo
// @author        Thaenz
// @license       GPLv3
// @license-link  https://www.gnu.org/licenses/gpl-3.0.txt
// @match         *://imgur.com/*
// @match         *://www.imgur.com/*
// @match         *://i.imgur.com/*
// @run-at        document-start
// @grant         none
// ==/UserScript==

(function()
{
	/* https://codeberg.org/video-prize-ranch/rimgo#instances */
	const RIMGO_INSTANCE = "rimgo.totaldarkness.net"

	url = location.href
	url = url.replace(/\bwww\.\b/, "")
	if (url.includes("i.imgur.com")) {
		url = url.replace("i.imgur.com", RIMGO_INSTANCE)
	} else {
		url = url.replace("imgur.com", RIMGO_INSTANCE)
	}

	location.href = url
})();

