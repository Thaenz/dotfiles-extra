// ==UserScript==
// @name          youtube to invidious
// @namespace     -
// @description   Redirects youtube to invidious
// @author        Thaenz
// @license       GPLv3
// @license-link  https://www.gnu.org/licenses/gpl-3.0.txt
// @match         *://youtube.com/*
// @match         *://www.youtube.com/*
// @match         *://music.youtube.com/*
// @match         *://m.youtube.com/*
// @match         *://youtu.be/*
// @match         *://www.youtu.be/*
// @run-at        document-start
// @grant         none
// ==/UserScript==

(function()
{
	/* https://api.invidious.io */
	const INVIDIOUS_INSTANCE = "yewtu.be"

	url = location.href
	url = url.replace(/\bwww\.\b/, "")
	if (url.includes("music.youtube.com")) {
		url = url.replace("music.youtube.com", INVIDIOUS_INSTANCE)

	} else if (url.includes("m.youtube.com")) {
		url = url.replace("m.youtube.com", INVIDIOUS_INSTANCE)

	} else if (url.includes("youtube.com")) {
		url = url.replace("youtube.com", INVIDIOUS_INSTANCE)

	} else {
		url = url.replace("youtu.be", INVIDIOUS_INSTANCE)
	}

	location.href = url
})();

