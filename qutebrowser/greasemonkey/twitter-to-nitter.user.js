// ==UserScript==
// @name          twitter to nitter
// @namespace     -
// @description   Redirects twitter to nitter
// @author        Thaenz
// @license       GPLv3
// @license-link  https://www.gnu.org/licenses/gpl-3.0.txt
// @match         *://twitter.com/*
// @match         *://www.twitter.com/*
// @match         *://mobile.twitter.com/*
// @run-at        document-start
// @grant         none
// ==/UserScript==

(function()
{
	/* https://github.com/zedeus/nitter/wiki/Instances */
	const NITTER_INSTANCE = "nitter.sneed.network"

	url = location.href
	url = url.replace(/\bwww\.\b/, "")
	if (url.includes("mobile.twitter.com")) {
		url = url.replace("mobile.twitter.com", NITTER_INSTANCE)
	} else {
		url = url.replace("twitter.com", NITTER_INSTANCE)
	}

	location.href = url
})();

