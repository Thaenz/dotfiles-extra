// ==UserScript==
// @name          quora to quetre
// @namespace     -
// @description   Redirects quora to quetre
// @author        Thaenz
// @license       GPLv3
// @license-link  https://www.gnu.org/licenses/gpl-3.0.txt
// @icon          https://libredd.it/favicon.ico
// @match         *://quora.com/*
// @match         *://www.quora.com/*
// @run-at        document-start
// @grant         none
// ==/UserScript==

(function()
{
	/* https://github.com/zyachel/quetre#instances */
	const QUETRE_INSTANCE = "quetre.iket.me"

	url = location.href
	url = url.replace(/\bwww\.\b/, "")
	url = url.replace("quora.com", QUETRE_INSTANCE)
	location.href = url
})();

