// ==UserScript==
// @name         StackExchange: No distrctions
// @namespace    -
// @version      0.2
// @description  Hide distracting elements from StackExchange network
// @author       Thaenz
// @match        *://*.stackexchange.com/*
// @match        *://stackoverflow.com/*
// @match        *://superuser.com/*
// @match        *://serverfault.com/*
// @match        *://mathoverflow.net/*
// @match        *://askubuntu.com/*
// @match        *://stackapps.com/*
// @grant        none
// ==/UserScript==

(function()
{
	const elements = [
		'.mb16', /* blogs & chat */
		'.js-consent-banner',
		'.js-sticky-leftnav',
		'#hot-network-questions',
	];

	for (let index = 0; index < elements.length; index++) {
		$(elements[index]).hide();
	}
})();

