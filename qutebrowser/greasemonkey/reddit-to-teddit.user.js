// ==UserScript==
// @name          reddit to teddit
// @namespace     -
// @description   Redirects reddit to teddit
// @author        Thaenz
// @license       GPLv3
// @license-link  https://www.gnu.org/licenses/gpl-3.0.txt
// @match         *://reddit.com/*
// @match         *://www.reddit.com/*
// @match         *://old.reddit.com/*
// @run-at        document-start
// @grant         none
// ==/UserScript==

(function()
{
	/* you can use teddit or libreddit whichever you prefer*/
	/* https://codeberg.org/teddit/teddit#instances */
	/* https://github.com/spikecodes/libreddit#instances */
	const TEDDIT_INSTANCE = "teddit.net"

	url = location.href
	url = url.replace(/\bwww\.\b/, "")
	if (url.includes("old.reddit.com")) {
		url = url.replace("old.reddit.com", TEDDIT_INSTANCE)
	} else {
		url = url.replace("reddit.com", TEDDIT_INSTANCE)
	}

	location.href = url
})();

