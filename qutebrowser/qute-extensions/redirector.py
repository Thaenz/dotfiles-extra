from operator import methodcaller
from qutebrowser.api import interceptor, message

# original code from: 
# gitlab.com/jgkamat/dotfiles/-/blob/master/qutebrowser/.config/qutebrowser/pyconfig/redirectors.py

REDIRECT_MAP = {

	# https://api.invidious.io
	"youtube.com": methodcaller('setHost', 'yewtu.be'),
	"www.youtube.com": methodcaller('setHost', 'yewtu.be'),
	"music.youtube.com": methodcaller('setHost', 'yewtu.be'),
	"m.youtube.com": methodcaller('setHost', 'yewtu.be'),
	"youtu.be": methodcaller('setHost', 'yewtu.be'),
	"www.youtu.be": methodcaller('setHost', 'yewtu.be'),

	# you can use teddit or libreddit whichever you prefer
	# https://codeberg.org/teddit/teddit#instances
	# https://github.com/spikecodes/libreddit#instances
	"reddit.com": methodcaller('setHost', 'teddit.net'),
	"www.reddit.com": methodcaller('setHost', 'teddit.net'),
	"old.reddit.com": methodcaller('setHost', 'teddit.net'),

	# https://codeberg.org/video-prize-ranch/rimgo#instances
    "imgur.com": methodcaller('setHost', 'rimgo.totaldarkness.net'),
    "www.imgur.com": methodcaller('setHost', 'rimgo.totaldarkness.net'),
    "i.imgur.com": methodcaller('setHost', 'rimgo.totaldarkness.net'),

	# https://github.com/zyachel/quetre#instances
    "quora.com": methodcaller('setHost', 'quetre.iket.me'),
    "www.quora.com": methodcaller('setHost', 'quetre.iket.me'),

	# https://github.com/zedeus/nitter/wiki/Instances
	"twitter.com": methodcaller('setHost', 'nitter.sneed.network'),
	"www.twitter.com": methodcaller('setHost', 'nitter.sneed.network'),
	"mobile.twitter.com": methodcaller('setHost', 'nitter.sneed.network'),

    # https://git.sr.ht/~edwardloveall/scribe/tree/HEAD/docs/instances.md
    "medium.com": methodcaller('setHost', 'scribe.rip'),
}

def int_fn(info: interceptor.Request):
	"""Block the given request if necessary."""
	if (info.resource_type != interceptor.ResourceType.main_frame or
			info.request_url.scheme() in {"data", "blob"}):
		return
	url = info.request_url
	redir = REDIRECT_MAP.get(url.host())
	if redir is not None and redir(url) is not False:
		message.info("Redirecting to " + url.toString())
		info.redirect(url)

interceptor.register(int_fn)

